name: inverse
layout: true
class: inverse

---
layout: false
background-image: url(img/fashion.jpeg)
background-size: contain
background-repeat: no-repeat
background-position: center right

.left-column[
# Common mistakes when moving to microservices
by Jakub Nabrdalik
]

---
layout: false

# whoami

jakub nabrdalik

solution architect & mentor @bottega

Solution Architect, Software Architect, Team Leader, Head of IT, Developer, etc

banking, fintech, ecommerce, telco, energy, startups

From 3 man-month systems, 200+ dev monoliths to 700+ microservices

60+ talks, 200+ workshops

DevoxxPL programme committee, ex WJUG, WGUG leader, etc.

details at [solidcraft.eu](http://solidcraft.eu)

---
template: inverse
background-image: url(img/bottega.png)
background-size: contain
class: middle

#whatis bottega

getting our clients up to speed

60 seniors (each 10+ years of industry experience)

11,000+ trained developers and attendees

300+ clients in the EU

15 books

400+ articles

---

# Architecture diagram

--

Kubernetes clusters with Istio and the usual

A big ass data lake

Scary, dockerized “Old/Core” system

Salesforce, SAP, a bunch of off-the-shelf products

Micro Frontends, web app, mobile app

Some databases

Errr.. where are the actual microservices?

--

background-image: url(img/microcloud.png)
background-repeat: no-repeat
background-position: bottom right
background-size: 50% auto

???

Infrastructure diagram with a “microservice cloud”, instead of containers and components. 

Same infrastructure as everyone else. No business domain at all. 

Developers do not know where to implement a new feature or how it fits the rest of the architecture. If you ask in one place you get differnt answers than in another. Lack of shared understanding.

---

background-image: url(img/eShopOnContainers.png)
background-size: contain
class: center, top

### If I'm lucky, I can get a team / responsibility chart

.footnote[https://github.com/dotnet-architecture/eShopOnContainers]
]



---

class: center, middle

# Architecture is the shared understanding of people on the project

???

architecture definition: “What is the shared understanding of senior people on the project”

Source of the problem: how the architecture is created in the organisation

---

background-image: url(img/infraKing.jpg)
background-repeat: no-repeat
background-size: contain
background-position: center right

.left-column[
# Infrastructure 
# Team
]

???

Infra is done by an infra team. Down to earth people, one level of abstraction for problems. They get it done right.

---

# Enterpise architect

> Enterprise architecture (EA) is **"a well-defined practice for conducting enterprise analysis, design, planning, and implementation, using a comprehensive approach at all times, for the successful development and execution of strategy."**

> [wikipedia](https://en.wikipedia.org/wiki/Enterprise_architecture)

---

background-image: url(img/dowant.jpg)
background-repeat: no-repeat
background-size: contain
background-position: center middle

.inverse[
# CTO be like 
]

---

background-image: url(img/bullshit.jpg)
background-repeat: no-repeat
background-size: contain
background-position: center middle

---
template: inverse
class: center, bottom
background-image: url(img/ea1.png)
background-repeat: no-repeat
background-size: contain

# Look Ma! No arrows

---

template: inverse
class: center, bottom
background-image: url(img/ea2.png)
background-repeat: no-repeat
background-size: contain

---

background-image: url(img/enterprisearch.jpg)
background-repeat: no-repeat
background-size: contain
background-position: center right

---

background-image: url(img/enterprisearch.jpg)
background-repeat: no-repeat
background-size: contain
background-position: center right

.left-column[

## EAs do NOT design software architecture

They design the enterprise

Best case scenario: 
> EAs are ignored by devs 

Worst case scenario: 
> devs play the game of bullshiting everyone

Most popular scenario: 
> blame game!

*EA is a high salary position, so EAs tend to try to keep the job no matter whether it helps or not*

]

---

template: inverse
background-image: url(img/snow.jpg)
background-repeat: no-repeat
background-size: cover
background-position: center middle

# Lead Architect

.footnote[archunit.org]


???

Architecture is organized around a lead architect. You can have a Lead architect for a 200 man monolith, because it’s easy to track changes. You cannot have only a Lead architect in a 200 man microservice oriented system, because you’ll not even notice when your architecture is broken. 

---

template: inverse
background-image: url(img/leadarch.jpg)
background-repeat: no-repeat
background-size: cover
background-position: center middle

# Lead Architect

Microservices - independently deployable

50 people == 10 teams == 40 git repos

People need to make decisions on the fly

You won't even know when it breaks

---

template: inverse
background-image: url(img/guild.jpg)
background-repeat: no-repeat
background-size: cover
background-position: center middle

#Architecture Guild

---

background-image: url(img/rpg.jpg)
background-repeat: no-repeat
background-size: cover
background-position: center middle

---

background-image: url(img/rpg.jpg)
background-repeat: no-repeat
background-size: cover
background-position: center middle

.inverse[
# How to recognize a dysfunctional guild?

Private communication channel

No outcomes

No practices

No responsibility

Nothing published
]

---

background-image: url(img/rpg.jpg)
background-repeat: no-repeat
background-size: cover
background-position: center middle

.inverse[
# How dysfunctional guilds end?
	
Management does not see any outcomes

Management sees no clear responsibility

Management loses trust (if they had any to begin with)

The guild gets disbanded

EA is hired again
]

---

# Architecture guild practices

”Architecture with 800 of My Closest Friends: The Evolution of Comcast’s Architecture Guild” by Jon Moore

https://www.infoq.com/articles/architecture-guild-800-friends/

---

background-image: url(img/vattenfallnuclear5.jpg)
background-repeat: no-repeat
background-size: contain
background-position: center middle

### Vattenfall: nuclear power plant in Sweden

???

The designer of this nuclear power plant sent a subtle message to climate change deniers

---

## Be responsible

Create a git repo for Architecture, give access to everyone

Decide what the guild does & write it down 
- knowledge sharing (diagrams etc.)
- common practices
- technical research, recommendations
- proactive changes to architecture (before development slows down)
- solving urgent issues

--

## Be responsive

Speker of the guild - on duty rotating role that
- ensures people get answers, decisions are made
- calls for working groups when urgent issues arise
- accountable for process and outcomes
- the nice Human Interface of the Guild

---

## Be inclusive

Invite everyone who is interested in architecture 

Accept all by default

--

## Be efficient

The Guild meets at least every sprint, runs through AP and non urgent issues

If there is an urgent issue, the Guild meets right away

For every problem/decision, create a workgroup (everyone interested)

The workgroup does research, publishes RFC in git repo

Everyone can comment, give people at least 1 week to process

When consensus is achieved, publish the recommendation (ADR)

---

# RFC & Architecture Decision Records

```
RFC TEMPLATE
- Context and Problem Statement
- RFC Drivers (a force, facing concern)
- Considered Options
- Pros and Cons of the Options
- Our recommendation
- Positive Consequences
- Negative Consequences
- Links
- Opened questions
```


```
ADR TEMPLATE
- Context: what information did we consider while making this decision?
- Alternatives: what options did we consider and why?
- Decision: what do we recommend?
- Rationale: why did we make that recommendation?
- Consequences: what are the known drawbacks?
```

https://github.com/joelparkerhenderson/architecture_decision_record


???

So how do you do it?

You need to follow practices, RFC, ADR, be inclusive. Architecture is about shared unerstanding. The Guild should be a guide, don’t think that it is a controlling body. Guilds thrive only in empower & trust culture. Command & Control management does not like it, does not understand it.

Alas, you cannot design a 700 microservice system top down. Whenever making an important decision (and architecture is often defined as the decisions that have a high cost of change), you need to include people that will implement it (live with its consequences). Making a design you don’t need to live with, is a sure way to fail. This is also the source of ivory tower architects. 

Last but not least, due to Conway’s Law, your org structure, the setup of your teams, will override any architecture you design.

So how to live with it? Start with designing your org departments, give them power to design their architecture. Communicate via events on a message bus (Kafka), to remove coupling. 

Inside each department use C4 model, make a repo with architecture, ask everyone to contribute via pull requests. Teach people the flavours of distributed systems, teach them how to validate and verify their architecture ideas.

You are going into microservices to improve productivity, which means you need to give more freedom and power to the people anyway. But giving people power without guidance and monitoring is anarchy, and cities build by anarchists kind of suck.

---

# Does anyone work like that?

.left-column[
Internet Engineering Task Force

Java Community Process

World Wide Web Consortium
]

.right-column[
For W3C:

Working draft (WD)

Candidate recommendation (CR)

Proposed recommendation (PR)

W3C recommendation (REC)

Later revisions
]


---

## Be transparent:

Have an open communication channel (slack/teams/etc)

Get a broadcast email, that forwards to all guild members

Every sprint a newsletter is published
- what the Guild did
- what the Guild is working on
- new RFCs to comment
- new ADRs written
- who is the Speaker of the Guild this sprint
- how you can contact the Guild

---

# Providing Guidance?

“The best architectures, requirements, and designs emerge from self-organizing teams." [Agile manifesto]

People who make decisions, and don’t have to live with the consequences, make stupid decisions.

You probably underestimate peer pressure

???

You need to include people that will implement it (live with its consequences). 
Making a design you don’t need to live with, is a sure way to fail. 
This is also the source of ivory tower architects. 

You are going into microservices to improve productivity, which means you need to give more freedom and power to the people anyway. But giving people power without guidance and monitoring is anarchy, and cities build by anarchists kind of suck.

---

background-image: url(img/softwarearchfordev.png)
background-repeat: no-repeat
background-size: contain
background-position: center right

.left-column[
# Give people <br /> guidance 

not everyone is on 

the same level

]

---

## What drivers architecture

Quality Attributes
- Maintainability
- Extensibility
- Performance
- Scalability
- Availability
- Security
- Flexibility

Constraints
- People
- Time and budget
- Technology
- Organisational

---

background-image: url(img/c4_png_component_task3.png)
background-repeat: no-repeat
background-size: contain
background-position: center middle

# Architecture diagram is a map (zoom in)

.footnote[
It leads to better decision making 
(c4model.com)
]
---

background-image: url(img/websequence.png)
background-repeat: no-repeat
background-size: 50%
background-position: center right

.left-column[ 
# Validation
Validate your static diagrams with dynamic sequence diagrams

websequencediagrams.com
]

---

.left-column[

# Cross cutting product teams?

]


---

background-image: url(img/angry.gif)
background-repeat: no-repeat
background-size: 50%
background-position: center right

.left-column[

# Cross cutting product teams?

]

---

background-image: url(img/angry.gif)
background-repeat: no-repeat
background-size: 50%
background-position: center right

.left-column[

# Cross cutting product teams?

“The Influence of Organizational Structure on Software Quality: An Empirical Case Study” by Nachiappan Nagappan, Brendan Murphy, Victor R. Basili [source](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/tr-2008-11.pdf)

“In our case study, the organizational metrics when applied to data from Windows Vista were statistically significant predictors of failure-proneness.”

“Conway's Law Is Real” by Kellen Evan [source](https://goodroot.ca/post/2018/10/13/practicality-metaphysics-conways-law/)

]

???

Last but not least, due to Conway’s Law, your org structure, the setup of your teams, will override any architecture you design.

So how to live with it? Start with designing your org departments, give them power to design their architecture. Communicate via events on a message bus (Kafka), to remove coupling. 

Inside each department use C4 model, make a repo with architecture, ask everyone to contribute via pull requests. Teach people the flavours of distributed systems, teach them how to validate and verify their architecture ideas.

You are going into microservices to improve productivity, which means you need to give more freedom and power to the people anyway. But giving people power without guidance and monitoring is anarchy, and cities build by anarchists kind of suck.

---

# Organizational failure metrics

“The greater the number of engineers that touched the code, the higher the failure rate.”

“The more edits to a component, the higher the instability and thus failure rate.”

“Components that were “owned” by a “master” – a single person with 75% or more of edits – had lower failure rates.”

“The lower the overall percentage of the organization that contributed to development, the lower the failure rate.”

“The more code contributors outside of the core working group, the higher the failure rate.”

“The ratio of “masters” making edits to code compared to the total number of engineers. The better the ratio in favour of “master” contribution, the lower the failure rate.”

---

# How to setup teams - constraints

Each codebase is changed by only one team (belongs to them, they have full control)

Each team can have several codebases

Cross cutting concerns (DBs, events, UX, monitoring) require a separate guild or a team

Teams should have to communicate as little as possible

Teams should have to coordinate as little as possible 

POs should communicate as much as possible

---

# Org structure vs architecture

Design your org structure, give each department power to design their architecture

Communicate between teams via an event bus to remove temporal coupling

Introduce consumer driven contracts

And so we proposed how the teams should be set up (management liked that)

---

# Clash of cultures

Two types of management culture in Europe (by Simon Brown)
- Command & Control (UK)
- Empowerment & Trust (North)

The former is problematic in a microservice world

---

background-image: url(img/deploymentmanager.png)
background-repeat: no-repeat
background-size: contain
background-position: center right

.left-column[
# Management wants to control deployment
]

???

Menagement wants to control deployment

Since we are talking about management, one common thing is the fear of continuous deployment. Or even the idea of managing and controlling deployments.

The definition of microservices: “Individually deployable”, without any syncrhonisation. 

If you are afraid of that, learn how to

Make your architecture resilient & event based

Use rolling deployments, A/B testing, automatic rollbacks

Learn to do monitoring well.

---

background-image: url(img/deploymentmanager.png)
background-repeat: no-repeat
background-size: contain
background-position: center right

.left-column[
The definition of microservices: 

“Individually deployable”

As in: without any synchronisation. 

Many small deployments remove big deployment risk

If you are afraid of that, learn how to make your architecture resilient & event based

Use rolling deployments, A/B testing, automatic rollbacks

Learn to do monitoring well
]


---

# QA focus shift

Young orgs: dev / test / UAT / stage / prod

Mature orgs: dev / prod 

You cannot have end-to-end tests in a microservice-based system

```
A(1 -> 2);  B(1 -> 2);  C(1 -> 2)

A2B2C2, A2B2C1, A2B1C2, A1B2C2, A1B2C1, A1B1C2, A2B1C1
```

---

# QA focus shift

Teach people how to write better tests

Work with devs using BDD/TDD

Use Consumer Driven Contracts to verify communication in local environment

Setup monitoring on production

Chaos engineering

???

Learn behaviour driven development and improve your single-microservice unit/integration testing skills

Use consumer driven contracts to verify contracts on a build (way before going to prod)

Learn to setup monitoring well. QA are great for this.

---

background-image: url(img/monitoring.jpg)
background-repeat: no-repeat
background-size: contain
background-position: center right

.left-column[
# Monitoring

Monitoring dashboard

Technical + business metrics

Debug readme - list of things to check/do when it fails at 3am

Do you know how to debug in production?
]

---

# Security risk

Old security mindset was to limit layers

If I can write code your server run, this server is mine anyway 

<div style="display: flex; align-items: center; justify-content: center;">
<img src="img/smurfs.jpeg" style="border-radius: 50%;" />
</div>

---

# How devs fool the security

Access only to "blessed" sites? - Internet via mobile

--

Only "blessed" dependencies? - artifacts on a pendrive + shadowing

--

Only "blessed" languages? - jar in PNG/JPG + Bootstrap

--

Do not make it hard for your people to do their work, they will hack you

If you run my code, I have the control. You cannot stop me.

Good Pull Request reviews + mandatory approvals + access to a my cluster only

---

# Microservices: security by vertical slices

Each team can have its own k8s cluster

The team OWNS the cluster (operations, on duty etc.)

Communication between team only via event bus

You can mess with your cluster, but you have no access to other team’s clusters

A resource group on Azure allows you to control billing

---

background-image: url(img/designingeventdrivensystems.png)
background-repeat: no-repeat
background-size: contain
background-position: center right

.left-column[

# Event based architecture

Distributed event logs

What was once a hard thing, or expensive to implement (read models), is now a one-liner

Resilient, faster, easier to reason about 

Async as default.

Do not go Event based only: it’s a trade-of
]

???

I give workshops. I teach a modular monolith. Then I teach synchronous distributed systems. Finally an event based system with a distributed event log. Design changes completely. 2 times last week I’ve heard: you’ve changed my perception 100%.

---

# Open Source > Closed cloud provided

Is Cloud provided PaaS cheaper?

Check how many devs and users it has

Open source K8s ~ 50k committers

Some Cloud services are driven by greed, not customer needs

CosmoDB makes you pay more due to wrong indexing policy

CosmoDB as a MongoDB substitute breaks at 500 inserts per second 

CosmoDB SLA: 8 weeks after reporting there is still no fix or workaround 

It’s often cheaper to own what’s crucial

???

Pivots of business vs team composition

---

# Architecture Guild after a year

We earned management trust

Knowledge is shared, cooperation is much better

All decisions and processes are transparent

Nobody is left alone, everyone has a say

Changes are done with proper research fist

We created IT strategy for the next year 
- 28 pages, font size 8
- 27 contributors in git
- constantly updated by everyone
- devs understand and agree to

---

class: center middle

## Good luck

> jakubn@gmail.com

This presentation is available at https://jakubn.gitlab.io/mistake2microservices
